import React from 'react'
import './myaccount.css'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useNavigate } from 'react-router-dom';
export default function Payout() {
    const payoutComplete = () => {
        alert("Your Payout Request is Succesfull");
    }
    const navigate = useNavigate();
    const toMyAccount = () => {
        navigate("/myaccount")
    }
  return (
    <div>
        <Card className='card1'>
            <Card.Body>
                <Card.Title>Payout</Card.Title>
                <Form onSubmit={payoutComplete}>
                    <Form.Group>
                        <Form.Label>Amount $</Form.Label>
                        <Form.Control required name='amount' type='number' placeholder='Amount in dollors'/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Payout Method</Form.Label>
                        <Form.Select aria-label='Choose method'>
                            <option>choose method</option>
                            <option value="bank">Bank Transfer</option>
                            <option value="cash">Collect as Cash</option>
                        </Form.Select>
                    </Form.Group>
                    
                    <Button variant='primary' type='submit'>Submit</Button>
                </Form>
            </Card.Body>
        </Card>
        <Button className='back2ac' variant='primary' onClick={toMyAccount}>Back to account</Button>
    </div>
  )
}
