import React from 'react'
import Home from '../pages/home/Home'
import MyAccount from '../pages/myAccount/MyAccount'
import { Route,Routes } from 'react-router-dom'
import Payout from '../pages/myAccount/Payout'
import Login from '../pages/login/Login'
export default function PrivateRouter() {
  return (
    <>
        <Routes>
            <Route path='/' element={<Home/>}/>
            <Route path='/myaccount' element={<MyAccount />} />
            <Route path='/payout' element={<Payout/>}/>
            <Route path='/login' element={<Login/>}/>
        </Routes>
    </>
  )
}
